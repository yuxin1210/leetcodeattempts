/**
 * 322. Coin Change
 * https://leetcode.com/problems/coin-change/tabs/description/
 */
public class Solution {
    public int coinChange(int[] coins, int amount) {
        if(amount<1){
            return 0;
        }
        int[] dp = new int[amount+1];
        dp[0] = 0;
        int sum = 0;
        //loop starts from sum=1
        while(++sum<=amount) {
            //System.out.println(sum);
            int min=-1;
            for(int coin: coins){
                if(sum>=coin && dp[sum-coin]!=-1){
                    int temp = dp[sum-coin]+1;
                    min = min < 0? temp: temp < min? temp:min;
                }
            }
            dp[sum]=min;
        }
        return dp[amount];
    }
}