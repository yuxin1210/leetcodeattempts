# 207: https://leetcode.com/problems/course-schedule/
class Solution(object):
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        conts = [[] for _ in xrange(numCourses)]      
        num_pres = [0] * numCourses

        for cont,pre in prerequisites:
            conts[pre].append(cont)
            num_pres[cont] += 1

        front = set()
        for i in xrange(numCourses):
            if num_pres[i] == 0:
                front.add(i)

        count = 0
        while front:
            next = set()
            for pre in front:
                count += 1
                for cont in conts[pre]:
                    num_pres[cont] -= 1
                    if num_pres[cont] == 0:
                        next.add(cont)
            front = next
        return count == numCourses