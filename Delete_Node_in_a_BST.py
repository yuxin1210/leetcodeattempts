# 450. Delete Node in a BST: https://leetcode.com/problems/delete-node-in-a-bst/description/

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def deleteNode(self, root, key):
        """
        :type root: TreeNode
        :type key: int
        :rtype: TreeNode
        """
        if not root:
            return None
        nodeFound,nodeFoundParent = Solution.searchNode(self,root,key,None)
        if nodeFound:
            children = []
            if nodeFound.left:
                children.append(nodeFound.left)
            if nodeFound.right:
                children.append(nodeFound.right)
            if len(children) == 0:
                if nodeFoundParent:
                    if nodeFound.val> nodeFoundParent.val:
                        nodeFoundParent.right = None
                    else:
                        nodeFoundParent.left = None
                else:
                    return None
            if len(children) == 1:
                if nodeFoundParent:
                    if nodeFound.val> nodeFoundParent.val:
                        nodeFoundParent.right = children[0]
                    else:
                        nodeFoundParent.left = children[0]
                else:
                    return children[0]
            if len(children) == 2:
                temp = nodeFound.left
                while(temp.right):
                    temp = temp.right
                temp.right = nodeFound.right.left
                nodeFound.right.left = nodeFound.left
                if nodeFoundParent:
                    if nodeFound.val> nodeFoundParent.val:
                        nodeFoundParent.right = nodeFound.right
                    else:
                        nodeFoundParent.left = nodeFound.right
                else:
                    return nodeFound.right
        return root               
    def searchNode(self,root,key,parent):
        if not root or root.val == key:
            return [root,parent]
        if root.val<key:
            return self.searchNode(root.right,key,root)
        else:
            return self.searchNode(root.left,key,root)
        
        