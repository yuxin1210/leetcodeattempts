# 207: https://leetcode.com/problems/course-schedule/
class Solution(object):
    def __init__(self):
        self.checked_classes = set()
        from collections import defaultdict 
        self.edges =  defaultdict(set)
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """     
        for course,pre in prerequisites:
            self.edges[course].add(pre)
        for course in range(numCourses):
            if course not in self.edges.keys():
                self.checked_classes.add(course)
        #print self.checked_classes
        for course in self.edges.keys():
            pres = set()
            pres.add(course)
            for pre in self.edges[course]:
                if pre not in self.checked_classes:
                    if not self.add_pre(self.edges,pres,pre):
                        return False
            self.checked_classes = self.checked_classes|pres
        return True 
    def add_pre(self,edges,pres,pre):
        if pres.intersection(self.edges[pre]):
            print "here"
            return False
        else:
            for p in self.edges[pre]:
                if p not in self.checked_classes:
                    pres.add(p)
                    if not self.add_pre(self.edges,pres,p):
                        return False
            return True